# Podpora tvorby dokumentů pro závody v orientačním běhu #
Tento nástroj vznikl jako semestrální práce předmětu BI-TEX v letním semestru ak. roku 2016/17. Umožňuje vytváření potřebných dokumentů při chodu závodu, jako startovní listiny, výsledky a seznam přihlášek.
## Obsah ##
Seznam souborů volně k dispozici:

* **OBdokumenty.rar** - obsahuje všechno potřebné ke zprovoznění nástroje (i návod).

* **navod.pdf** - dokumentace, jak správně nástroj používat.

* **startovka_web.pdf** - příklad použití na vytvoření startovní listiny, určené pro umístění na web.

* **vysledky_tisk.pdf** - příklad použití na výsledky k tisku.

Soubory se nachází v sekci [Downloads](https://bitbucket.org/vojtanos/tex/downloads/).

Zdrojové .tex kódy najdete v sekci [Source](https://bitbucket.org/vojtanos/tex/src).
## Použití##
Všechny potřebné informace jak nástroj používat se dočtete v návodu. 

Příklady použití na již proběhlých závodech:

* Přebor škol v orientačním běhu, 9. 5. 2017. [Startovky](http://obkotlarka.cz/zavody/kk-ps-ob-2017/startovka) (ze starší verze)

* Pražský pohár žactva, 14. 6. 2017. [Výsledky](http://obkotlarka.cz/assets/files/prebor2017/vysledky_ppz.pdf) (nový design)

## Updates ##

Zatím je k dispozici pouze jeden typ designu pro tiskovou verzi a jeden pro webovou. Časem přidělám další možné typy šablon, které by se daly použít a nástroj tomu přizpůsobím.